# ArduRGB
ArduRGB aims to be a simple to use alternative to proprietary RGB solutions powered by an Arduino microprocessor.

[![Demo video of ArduRGB](https://gitlab.com/tenten8401/ardurgb/raw/master/preview.png)](https://www.youtube.com/watch?v=cqb8LcBHWHQ)

# Known issues
- Needs some more effects
- Gitlab CI autobuilding for the GUI would be nice

# Building
#### Arduino Code
1. Open with PlatformIO IDE of your choice or CLI
2. Connect ESP32 to computer
3. Run `platformio run --target upload` or click run icon in IDE

#### Qt GUI
1. Open in Qt Creator or terminal
- (GUI) Click "Build" in Qt Creator
- (Term) `mkdir build && cd build && qmake ../ && make -j4`