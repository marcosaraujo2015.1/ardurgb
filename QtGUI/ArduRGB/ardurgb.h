#ifndef ARDURGB_H
#define ARDURGB_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>

namespace Ui {
class ArduRGB;
}

class ArduRGB : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArduRGB(QWidget *parent = nullptr);
    ~ArduRGB();
    QSerialPort serial;

private slots:
    void on_connectButton_clicked();
    void HandleReadyRead();

private:
    Ui::ArduRGB *ui;
    void ScanSerialPorts();
    void CleanUI();
};

#endif // ARDURGB_H
