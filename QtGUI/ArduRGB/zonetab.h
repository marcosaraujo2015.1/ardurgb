#ifndef ZONETAB_H
#define ZONETAB_H

#include <QWidget>
#include <QSerialPort>
#include <QMessageBox>

namespace Ui {
class ZoneTab;
}

class ZoneTab : public QWidget
{
    Q_OBJECT

public:
    explicit ZoneTab(int zone, int hue, int saturation, int value, int mode, int delay, QSerialPort& serial, QWidget *parent = nullptr);
    ~ZoneTab();

private slots:
    void on_updateButton_clicked();

    void on_hueSlider_valueChanged();

    void on_saturationslider_valueChanged();

    void on_valueSlider_valueChanged();

    void on_colorValueEdit_editingFinished();

private:
    Ui::ZoneTab *ui;
    void updateModeDropdown();
    void updateColorPreview();
    void updateColorSliders();
    QColor color;
    int zone;
    QSerialPort* serial;
};

#endif // ZONETAB_H
