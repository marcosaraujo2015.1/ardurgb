#include "ardurgb.h"
#include "ui_ardurgb.h"
#include "zonetab.h"

extern QSerialPort serial;

ArduRGB::ArduRGB(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ArduRGB)
{
    ui->setupUi(this);

    // Scan serial ports & add to combobox
    ScanSerialPorts();

    // Tell "Connected" checkbox to ignore mouse events
    ui->connectedCheckBox->setAttribute(Qt::WA_TransparentForMouseEvents);

    // Attach serial port reader
    connect(&serial, SIGNAL(readyRead()), this, SLOT(HandleReadyRead()));
}

// Re-Scans serial ports and adds them to menu
void ArduRGB::ScanSerialPorts() {
    ui->serialComboBox->clear();
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->serialComboBox->addItem(info.portName());
    }
}


ArduRGB::~ArduRGB()
{
    delete ui;
}

void ArduRGB::CleanUI() {
    ui->deviceTable->setRowCount(0);
    int tabs = ui->tabWidget->count() - 1;
    for(int i = 0; i < tabs; i++) {
        ui->tabWidget->removeTab(1);
    }
}

void ArduRGB::HandleReadyRead() {
    while(serial.canReadLine()) {
        QByteArray data = serial.readLine().trimmed();
        QList<QByteArray> splitData = data.split('|');
        qInfo() << "Hello!";
        if(splitData[0] == "ArduRGB-1.0") {
            // FW Validated, ask for devices & zones
            CleanUI();
            serial.write("load_devices\n");
            serial.write("load_zones\n");
        } else if(splitData[0] == "device" && splitData.size() == 5) {
            // Load device to menu
            int device = splitData[1].toInt();

            QString deviceName = splitData[2];
            ui->deviceTable->insertRow(device);
            ui->deviceTable->setItem(device, 0, new QTableWidgetItem());
            ui->deviceTable->item(device, 0)->setText(deviceName);

            int deviceZone = splitData[3].toInt();
            ui->deviceTable->setItem(device, 1, new QTableWidgetItem());
            ui->deviceTable->item(device, 1)->setText(QString::number(deviceZone));
        } else if(splitData[0] == "zone" && splitData.size() == 7) {
            int zone = splitData[1].toInt();
            int hue = splitData[2].toInt();
            int saturation = splitData[3].toInt();
            int value = splitData[4].toInt();
            int mode = splitData[5].toInt();
            int delay = splitData[6].toInt();

            qInfo() << data;
            //qInfo() << QString("%1, %2, %3, %4, %5, %6").arg(zone).arg(hue).arg(saturation).arg(value).arg(mode).arg(delay);

            ui->tabWidget->addTab(new ZoneTab(zone, hue, saturation, value, mode, delay, serial), QString("Zone %1").arg(zone));
        }
    }
}


void ArduRGB::on_connectButton_clicked()
{
    if(!serial.isOpen()) {
        serial.setPortName(ui->serialComboBox->currentText());
        serial.setBaudRate(QSerialPort::Baud9600);
        if(serial.open(QIODevice::ReadWrite)) {
            // Perform connection routine
            ui->serialComboBox->setEnabled(false);
            ui->connectedCheckBox->setChecked(true);
            ui->connectButton->setText("Disconnect");

            // Wait for connection to finish and ask for FW version
            QTimer::singleShot(500, [=](){
                serial.write("fw\n");
            });
        } else {
            CleanUI();
            QMessageBox msg;
            msg.setText("Failed to connect to serial port");
            msg.exec();
            ui->serialComboBox->setEnabled(true);
            ui->connectedCheckBox->setChecked(false);
            ui->connectButton->setText("Connect");
        }
    } else {
        CleanUI();
        ui->serialComboBox->setEnabled(true);
        ui->connectButton->setText("Connect");
        ui->connectedCheckBox->setChecked(false);
        serial.close();
    }
}
