#include "ardurgb.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    // Seed rng
    unsigned int time_ui = static_cast<unsigned int>( time(nullptr) );
    srand( time_ui );

    // Show window
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);
    ArduRGB w;
    w.show();
    return a.exec();
}
