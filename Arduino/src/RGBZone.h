#pragma once
#include <Arduino.h>

class RGBZone {
public:
  int hue;
  int saturation;
  int value;
  int mode;
  int delay;
};