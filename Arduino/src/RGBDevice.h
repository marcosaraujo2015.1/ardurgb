#pragma once
#include <Arduino.h>

class RGBDevice {
public:
  String name;
  int zone;
  bool addressible;
  int leds;
};