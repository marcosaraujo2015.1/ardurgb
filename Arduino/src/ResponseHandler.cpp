#include <Configuration.h>
#include <RGBZone.h>
#include <ResponseHandler.h>

bool ResponseHandler::getResponse(String response, RGBZone *zones,
                                  RGBDevice *devices) {
  bool validResponse = true;
  response.trim();
  String req = getValue(response, '|', 0);
  int zone = getValue(response, '|', 1).toInt();
  int hue = getValue(response, '|', 2).toInt();
  int saturation = getValue(response, '|', 3).toInt();
  int value = getValue(response, '|', 4).toInt();
  int mode = getValue(response, '|', 5).toInt();
  int delay = getValue(response, '|', 6).toInt();
  if (req == "update") {
    if (zone <= NUM_ZONES && zone >= 0) {
      if (hue <= 255 && hue >= 0) {
        zones[zone].hue = hue;
      } else {
        validResponse = false;
      }
      if (saturation <= 255 && saturation >= 0) {
        zones[zone].saturation = saturation;
      } else {
        validResponse = false;
      }
      if (value <= 255 && value >= 0) {
        zones[zone].value = value;
      } else {
        validResponse = false;
      }
      if (mode <= 10 && mode >= 0) {
        zones[zone].mode = mode;
      } else {
        validResponse = false;
      }
      if (delay <= 100 && delay >= 1) {
        zones[zone].delay = delay;
      } else {
        validResponse = false;
      }
    } else {
      validResponse = false;
    }
    Serial.println("ok");
  } else if (req == "load_zones") {
    for (int i = 0; i < NUM_ZONES; i++) {
      Serial.println("zone|" + String(i) + "|" + String(zones[i].hue) + "|" +
                     String(zones[i].saturation) + "|" +
                     String(zones[i].value) + "|" + String(zones[i].mode) +
                     "|" + String(zones[i].delay));
    }
  } else if (req == "load_devices") {
    for (int i = 0; i < NUM_DEVICES; i++) {
      Serial.println("device|" + String(i) + "|" + String(devices[i].name) +
                     "|" + String(devices[i].zone) + "|" +
                     String(devices[i].addressible));
    }
  } else if (req == "fw") {
    Serial.println("ArduRGB-1.0");
  } else {
    validResponse = false;
  }
  return validResponse;
}

String ResponseHandler::getValue(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}